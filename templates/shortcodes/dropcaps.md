<p class="drop-caps" data-letter="{{letter}}">
	{{ body | safe }}
</p>
