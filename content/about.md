+++
title = "About"
template = "about.html"
+++

Sometimes I do mathematics.  
Sometimes I write LaTeX.  
Sometimes I look at fonts.  
Sometimes I program.  

This website is created with the help of the static site generator [Zola][1].

The fonts used on this website are [EB Garamond][2] (licensed under [OFL 1.1][3]) and [Libertinus][4] (licensed under [OFL 1.1][5]).
The logos in the footer of this website are taken from [Font Awesome][6] (licensed under [CC BY 4.0][7]).
The ornaments used on this site are in part taken from the LaTeX package [pfgornaments][8] (licensed under [LPPL 1.3c][9]) via [SVGornaments][10].

[1]:  https://www.getzola.org/
[2]:  https://en.wikipedia.org/wiki/EB_Garamond
[3]:  https://github.com/octaviopardo/EBGaramond12/blob/master/OFL.txt
[4]:  https://github.com/alerque/libertinus/
[5]:  https://github.com/alerque/libertinus/blob/master/OFL.txt
[6]:  https://fontawesome.com/
[7]:  https://creativecommons.org/licenses/by/4.0/
[8]:  https://ctan.org/pkg/pgfornament/
[9]:  https://ctan.org/license/lppl1.3/
[10]: https://gitlab.com/cionx/svgornaments/
