+++
title = "Writing"
template = "writing.html"
+++

## Lecture notes ##

I sometimes try to write lecture notes.
Below are some attempts.

### Foundations of Representation Theory ###

These are unofficial notes for an eponymous lecture course that was given by Dr. Hans Franzen at the University of Bonn in the winter term of 2018/19.
These notes are mostly complete.
The LaTeX code and a compiled version of the notes are available on [GitLab][1].

### Algebra I ###

These notes are based on an eponymous lecture that was given by Prof. Catharina Stroppel in the summer term of 2014 at the University of Bonn.
The notes contain everything that happened in the course, and some more.
The LaTeX code and a compiled version of the notes are available on [GitLab][2].

### Lie algebras ###

These notes are based on various lectures, as well as some books.
The notes are in a slow process of being reworked, and are currently missing lots of content.
The LaTeX code and a compiled version of the current version of the notes are available on [GitLab][3].



## Review courses ##

I gave some review courses in linear algebra and introductory algebra in the University of Bonn.
These courses took place over the span of four days.
Each day consisted of a two-hour review lecture followed by a two-hour exercise session.

These review courses were commissioned by the [Fachschaft Mathematik][4].
The materials for these review courses are completely in German.

### Lineare Algebra II ###

I gave two review courses for the lecture course Lineare Algebra II (linear algebra 2).
One for the lecture course given by Prof. Jens Franke in the summer term of 2016, and one following the lecture course given by Prof. Catharina Stroppel in the summer term of 2017.
The materials for these review courses are available on [GitLab][5] and on [GitLab][6].

### Einführung in die Algebra ###

I also gave two review courses for the lecture course Einführung in die Algebra (introduction to algebra).
One for the lecture course given by Prof. Catharina Stroppel in the winter term of 2017/18, and one for the lecture course given by Prof. Jan Schröer in the winter term of 2019/2020.
The materials for these review courses are available on [GitLab][7] and on [GitLab][8].



## Others ##

### Writing LaTeX ###

I’ve written down some notes on writing mathematical texts with LaTeX.
The text and its source code are available on [GitLab][9].

### BibLaTeX Entries ###

I am maintaining a sometimes growing list of bibliographic entries to be used with BibLaTeX.
The list is available on [GitLab][10].




[1]:  https://gitlab.com/lecture-notes-bonn/original/foundations-in-representation-theory-notes-ws-18-19/
[2]:  https://gitlab.com/lecture-notes-bonn/original/algebra-1-notes-ss-14/
[3]:  https://gitlab.com/lecture-notes-bonn/original/lie-algebras/
[4]:  https://fsmath.uni-bonn.de/home.html
[5]:  https://gitlab.com/lecture-notes-bonn/review-courses/lineare-algebra-2-review-ss-16
[6]:  https://gitlab.com/lecture-notes-bonn/review-courses/lineare-algebra-2-review-ss-17
[7]:  https://gitlab.com/lecture-notes-bonn/review-courses/einfuehrung-in-die-algebra-review-ws-17-18
[8]:  https://gitlab.com/lecture-notes-bonn/review-courses/einfuehrung-in-die-algebra-review-ws-19-20
[9]:  https://gitlab.com/cionx/writing-latex/
[10]: https://gitlab.com/cionx/bibliography/
