+++
title = "Semidirect products as group objects I"
date = 2022-03-12
[taxonomies]
tags = ["mathematics", "semidirect products", "group objects"]
+++

{% dropcaps(letter="Y") %}
	<span class="first-sentence">Yesterday, I was watching the talk <a href="https://youtu.be/-_Yg-A8_lIY?t=830">“A Short Introduction to Categorical Logic”</a></span> by Evan Patterson on YouTube.
	Around the midpoint of the talk, some examples for group objects are mentioned.
	I was aware of the first three examples (groups, topological groups, and Lie groups), but I hadn’t seen the last example before:
	apparently, given a group <span class="math">G</span>, group objects in the category of <span class="math">G</span>-sets are (in some sense) the same semidirect products of the form <span class="math">(–) ⋊ G</span>.
{% end %}

Let us check this.

A group object in the category of <span class="math">G</span>-sets is — by definition — a <span class="math">G</span>-set <span class="math">H</span> together with a <span class="math">G</span>-equivariant multiplication map <span class="math">μ : H × H → H</span>, a <span class="math">G</span>-equivariant inversion map <span class="math">ι : H → H</span>, and a <span class="math">G</span>-equivariant map <span class="math">\{★\} → H</span> picking out an element <span class="math">e</span> of <span class="math">H</span>, such that these maps make <span class="math">H</span> into a group.
(More precisely, the map <span class="math">μ</span> is the multiplication on <span class="math">H</span>, the map <span class="math">ι</span> assigns to each element of <span class="math">H</span> its inverse, and the element <span class="math">e</span> is the neutral element for the group structure.)

The forgetful functor from the category of <span class="math">G</span>-sets to the category of sets preserves products, and therefore preserves group objects.
The group object <span class="math">H</span> is therefore the same as a group that is also a <span class="math">G</span>-set and for which

- we have <span class="math">g . (h₁ · h₂) = (g . h₁) · (g . h₂)</span>,
- we have <span class="math">g . e = e</span>, and
- we have <span class="math">(g . h)⁻¹ = g . h⁻¹</span>.

The first condition tells us that <span class="math">G</span> needs to act on <span class="math">H</span> via group automorphisms, and the second and third condition follows from this.

We hence find that a group object in the category of <span class="math">G</span>-sets is the same as a group <span class="math">H</span> together with a homorphism of groups from <span class="math">G</span> to <span class="math">Aut(H)</span>.
This is, in turn, the same as a semidirect product of the form <span class="math">H ⋊ G</span>.
