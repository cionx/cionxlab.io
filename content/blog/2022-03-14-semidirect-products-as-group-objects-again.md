+++
title = "Semidirect products as group objects II"
date = 2022-03-14
[taxonomies]
tags = ["mathematics", "semidirect products", "group objects", "category theory"]
+++

{% dropcaps(letter="I") %}
	<span class="first-sentence">I kept thinking about <a href="../semidirect-products-as-group-objects">the last post</a></span> and realized a more systematic explanation:
	We have two kinds of procedures that we can apply to the category of sets, and more generally to any kind of category (that admits finite products).
	On the one hand, we can form the category of <span class="math">G</span>-sets, which is the functor category <span class="math">[G, Set]</span>.
	On the other hand, we can form the category of groups, which is the category of group objects in <span class="math">Set</span>.
	Both of these constructions commute because products in functor categories can be computed pointwise.
	In this way, we will end up with an equivalence between the functor category <span class="math">[G, Grp]</span>, whose objects are the semidirect products of the form <span class="math">(–) ⋊ G</span>, and the category of group objects in the category of <span class="math">G</span>-Sets.
	Let us be a bit more precise:
{% end %}

We denote the category of groups by <span class="math">Grp</span> and the category of sets by <span class="math">Set</span>.
Let <span class="math">𝒞</span> be a category admitting finite products, and let us denote by <span class="math">Grp(𝒞)</span> the resulting category of group objects in <span class="math">𝒞</span>.
We have an equivalence of categories between <span class="math">Grp</span> and <span class="math">Grp(Set)</span>.

Given another category <span class="math">𝒢</span>, the resulting functor category <span class="math">[𝒢, 𝒞]</span> again admits finite products.
Moreover, these products can be computed pointwise.
It follows that we have an equivalence of categories between <span class="math">[𝒢, Grp(𝒞)]</span> and <span class="math">Grp([𝒢, 𝒞])</span>.

We may regard the group <span class="math">G</span> as a one-object groupoid.
The category of <span class="math">G</span>-sets, which we denote by <span class="math">G-Set</span>, is then equivalent to the functor category <span class="math">[G, Set]</span>.

In my original post, I regarded as “semidirect product of the form <span class="math">(–) ⋊ G</span>” as a pair <span class="math">(H, θ)</span> consisting of a group <span class="math">H</span> and a homomorphism of groups <span class="math">α</span> from <span class="math">G</span> to <span class="math">Aut(H)</span>.
The automorphism group <span class="math">Aut(H)</span> is precisely the group of automorphisms of <span class="math">H</span> as an object of the category <span class="math">Grp</span>.
Such a pair <span class="math">(H, α)</span> is therefore the same as a functor from <span class="math">G</span> to <span class="math">Grp</span>.
We can now apply the equivalences of categories
{% equation() %}
[G, Grp]
≃
[G, Grp(Set)]
≃
Grp([G, Set])
≃
Grp(G-Set)
{% end %}
to see that a functor from <span class="math">G</span> to <span class="math">Grp</span> is essentially the same as a group object in the category of <span class="math">G</span>-sets.
